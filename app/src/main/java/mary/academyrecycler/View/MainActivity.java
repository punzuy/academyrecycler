package mary.academyrecycler.View;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import mary.academyrecycler.Model.POJO.User;
import mary.academyrecycler.Presenter.MainPresenter;
import mary.academyrecycler.R;

public class MainActivity extends AppCompatActivity implements IMainActivity {

    MainPresenter mainPresenter;
    private ProgressBar spinner;
    private TextView noInternet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = findViewById(R.id.spinner);
        noInternet = findViewById(R.id.noInternet);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            noInternet.setVisibility(View.GONE);
            if (mainPresenter == null) {
                mainPresenter = new MainPresenter(this);
            }
        } else {
            showNoInternetMessage();
        }
    }

    @Override
    public void showCards(List<User> users) {
        hideSpinner();
        RecyclerView usersRecycler = findViewById(R.id.usersRecycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        usersRecycler.setLayoutManager(linearLayoutManager);
        usersRecycler.setAdapter(new UsersAdapter(mainPresenter, users));
    }

    @Override
    public void showSpinner() {
        spinner.setVisibility(View.VISIBLE);
    }

    public void hideSpinner() {
        spinner.setVisibility(View.GONE);
    }

    public void showNoInternetMessage() {
        hideSpinner();
        noInternet.setVisibility(View.VISIBLE);
    }


    public void onClickGirls(View view) {
        findViewById(R.id.girls).setBackgroundColor(getResources().getColor(R.color.colorAccent));
        findViewById(R.id.guys).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        findViewById(R.id.both).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mainPresenter.onClickedGirls();
    }

    public void onClickGuys(View view) {
        findViewById(R.id.girls).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        findViewById(R.id.guys).setBackgroundColor(getResources().getColor(R.color.colorAccent));
        findViewById(R.id.both).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mainPresenter.onClickedGuys();
    }

    public void onClickBoth(View view) {
        findViewById(R.id.girls).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        findViewById(R.id.guys).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        findViewById(R.id.both).setBackgroundColor(getResources().getColor(R.color.colorAccent));
        mainPresenter.onClickedBoth();
    }

    public void goToUserActivity(User user) {
        Intent intent = new Intent(MainActivity.this, UserActivity.class);
        intent.putExtra("firstname", user.getName().getFirst());
        intent.putExtra("lastname", user.getName().getLast());
        intent.putExtra("image", user.getPicture().getLarge());
        startActivity(intent);
    }
}
