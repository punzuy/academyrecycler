package mary.academyrecycler.View;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mary.academyrecycler.Model.POJO.User;
import mary.academyrecycler.Presenter.MainPresenter;
import mary.academyrecycler.R;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private List<User> users;
    private final MainPresenter presenter;

    UsersAdapter(MainPresenter presenter, List<User> users) {
        this.presenter = presenter;
        this.users = users;
    }

    @NonNull
    @Override
    public UsersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card, parent, false);
        return new UsersAdapter.ViewHolder(presenter, v);
    }

    @Override
    public void onBindViewHolder(@NonNull UsersAdapter.ViewHolder holder, int position) {
        final User user = users.get(position);

        Picasso
                .get()
                .load(user.getPicture().getLarge())
                .into(holder.image);

        holder.firstName.setText(user.getName().getFirst());
        holder.lastName.setText(user.getName().getLast());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView firstName;
        TextView lastName;

        ViewHolder(final MainPresenter presenter, View v) {
            super(v);
            image = v.findViewById(R.id.image);
            firstName = v.findViewById(R.id.firstName);
            lastName = v.findViewById(R.id.lastName);
            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onItemClickedAtPosition(getAdapterPosition());
                }
            });
        }
    }
}
