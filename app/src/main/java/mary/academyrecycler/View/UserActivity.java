package mary.academyrecycler.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import mary.academyrecycler.R;

public class UserActivity extends AppCompatActivity {

    TextView firstName;
    TextView lastName;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle userInfo = getIntent().getExtras();
        setContentView(R.layout.activity_user);

        firstName = findViewById(R.id.firstNameUser);
        lastName = findViewById(R.id.lastNameUser);
        image = findViewById(R.id.imageUser);

        firstName.setText(userInfo.getString("firstname"));
        lastName.setText(userInfo.getString("lastname"));
        Picasso
                .get()
                .load(userInfo.getString("image"))
                .into(image);
    }

    public void onClickBackArrow(View v) {
        onBackPressed();
    }
}
