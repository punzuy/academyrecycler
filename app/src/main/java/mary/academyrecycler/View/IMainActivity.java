package mary.academyrecycler.View;

import java.util.List;

import mary.academyrecycler.Model.POJO.User;

public interface IMainActivity {

    void showCards(List<User> users);
    void showSpinner();
    void goToUserActivity(User user);
}
