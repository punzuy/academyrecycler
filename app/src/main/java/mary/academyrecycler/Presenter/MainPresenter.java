package mary.academyrecycler.Presenter;

import java.util.ArrayList;
import java.util.List;

import mary.academyrecycler.Model.NetworkModule;
import mary.academyrecycler.Model.POJO.UsersData;
import mary.academyrecycler.Model.POJO.User;
import mary.academyrecycler.View.IMainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static mary.academyrecycler.Model.NetworkModule.BOTH;
import static mary.academyrecycler.Model.NetworkModule.GIRLS;

public class MainPresenter {

    private final NetworkModule networkModule;
    private final IMainActivity iMainActivity;
    private List<User> users;

    public MainPresenter(IMainActivity iMainActivity) {
        this.iMainActivity = iMainActivity;
        networkModule = new NetworkModule();
        getUsersData(BOTH);
    }

    private Callback callback = new Callback<UsersData>() {
        @Override
        public void onResponse(Call<UsersData> call, Response<UsersData> response) {
            UsersData usersData = response.body();

            users = new ArrayList<>(usersData.getUsers());
            iMainActivity.showCards(users);
        }

        @Override
        public void onFailure(Call<UsersData> call, Throwable t) {
        }
    };

    public void getUsersData(String gender) {
        iMainActivity.showSpinner();
        Call<UsersData> usersData = networkModule.getUsers(gender);
        usersData.enqueue(callback);
    }

    public void onClickedGirls() {
        getUsersData(GIRLS);
    }

    public void onClickedGuys() {
        getUsersData(NetworkModule.GUYS);
    }

    public void onClickedBoth() {
        getUsersData(BOTH);
    }

    public void onItemClickedAtPosition(int position) {
        iMainActivity.goToUserActivity(users.get(position));
    }


}
