package mary.academyrecycler.Model;

import mary.academyrecycler.Model.POJO.UsersData;
import retrofit2.Call;
import retrofit2.http.GET;

public interface APIService {

    @GET("?results=40")
    Call<UsersData> getUsers();

    @GET("?gender=female&results=20")
    Call<UsersData> getGirls();

    @GET("?gender=male&results=20")
    Call<UsersData> getGuys();


}

