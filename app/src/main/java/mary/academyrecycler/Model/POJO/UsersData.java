package mary.academyrecycler.Model.POJO;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

    public class UsersData {

        @SerializedName("results")
        @Expose
        private List<User> users = null;

//        @SerializedName("info")
//        @Expose
//        private Info info;


        public List<User> getUsers() {
            return users;
        }

        public void setUsers(List<User> users) {
            this.users = users;
        }


//        public Info getInfo() {
//            return info;
//        }
//
//        public void setInfo(Info info) {
//            this.info = info;
//        }

    }

