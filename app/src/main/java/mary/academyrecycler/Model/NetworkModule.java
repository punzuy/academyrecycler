package mary.academyrecycler.Model;

import mary.academyrecycler.Model.POJO.UsersData;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkModule {

    private static final String BASE_URL = "https://randomuser.me/api/";
    public static final String GIRLS = "girls";
    public static final String GUYS = "guys";
    public static final String BOTH = "both";

    public Call<UsersData> getUsers(String gender) {
        Retrofit.Builder retrofitBuilder =
                new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = retrofitBuilder.build();
        APIService APIService = retrofit.create(APIService.class);

        switch (gender) {
            case GIRLS:
                return APIService.getGirls();
            case GUYS:
                return APIService.getGuys();
            default:
                return APIService.getUsers();

        }
    }
}
